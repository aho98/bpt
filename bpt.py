# -*- coding: utf-8 -*-
"""
Created on Sun Oct 30 13:50:02 2016

@author: alena
"""


import matplotlib.pyplot as plt

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

N = 929156

data = [0] * N
for i in range(N):
    data[i] = [0] * 8
k = 0
flag = True

with open("bpt_subset.csv") as file:
    for i in range(N):
        line = file.readline()
        line = list(line.split(','))
        flag = True
        if len(line) == 8:
            for j in range(8):
                if isfloat(line[j]):
                    data[k][j] = float(line[j])
                else:
                    flag = False
                    break
                if (data[k][j] == 0.) or (data[k][j] == 0):
                    flag = False
                    break
            if flag:
                k += 1

x = [0] * k
y = [0] * k
j = 0
fig = plt.figure() 
             
for i in range(k):
    sn1 = data[i][0] / data[i][1]
    sn2 = data[i][2] / data[i][3]
    sn3 = data[i][4] / data[i][5]
    sn4 = data[i][6] / data[i][7]
    if (sn1 > 3) and(sn2 > 3) and(sn3 > 3) and (sn4 > 3):
        y[j] = data[i][2] / data[i][0]
        x[j] = data[i][6] / data[i][4]
        j += 1

plt.plot(x, y, 'ro')
plt.semilogx()
plt.semilogy()
plt.xlabel('NII / Hβ')
plt.ylabel('OIII / Hα')
plt.savefig('2')              
plt.show()
